package worldcupsweep

import worldcupsweep.teams.Team

case class Person(name: String, teams: List[Team] = List())
