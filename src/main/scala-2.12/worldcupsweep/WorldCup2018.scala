package worldcupsweep

import worldcupsweep.teams.{TeamGenerator, Teams}

object WorldCup2018 extends App {

  import TeamGenerator._

  val sweep = Teams().assignTeams(args.map(Person(_)).toList)

  sweep.people foreach ( person =>
    println(s"${person.name} has got these teams: ${person.teams.mkString(", ")}")
  )
  if(sweep.teams.nonEmpty){
    println(s"Teams remaining: ${sweep.teams.mkString(", ")}")
  }

}
