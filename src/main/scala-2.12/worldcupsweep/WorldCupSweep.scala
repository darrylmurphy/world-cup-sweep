package worldcupsweep

import worldcupsweep.teams.Team

trait WorldCupSweep {

  val people: List[Person]

  val teams: Seq[Team]

}