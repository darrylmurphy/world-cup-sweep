package worldcupsweep.teams

import worldcupsweep.{Person, WorldCupSweep}

import scala.annotation.tailrec

trait Team

object TeamGenerator {
  implicit class Generator(teams: Seq[Team]){
    def generate: Team = {

      val index = new scala.util.Random().nextInt(teams.length)

      if(index > 0){
        teams(index)
      } else {
        teams.head
      }

    }

    def assignTeams(people: List[Person]): WorldCupSweep = {

      val teamsRemaining = 32 % people.length
      val teamsToAssign = (32 - teamsRemaining) / people.length

      @tailrec
      def iterate(teams: Seq[Team] = teams, personIndex: Int = 0, people: List[Person] = people): WorldCupSweep = {

        val person = people(personIndex)
        val personTeams = person.teams
        val nextPerson = personIndex + 1
        val peopleToIterate = nextPerson < people.length

        val team = teams.generate
        val removeTeam = teams.filterNot(_.equals(team))

        val addTeamToPerson = person.copy(teams = personTeams ++ Seq(team))

        val updatedPeople = people.splitAt(personIndex) match {
          case (a, b) => a ++ Seq(addTeamToPerson) ++ b.tail
        }

        val teamsToIterate = teams.length > 1

        if(teamsToIterate){
          if(addTeamToPerson.teams.length < teamsToAssign){
            iterate(removeTeam, personIndex, updatedPeople)
          } else if (peopleToIterate) {
            iterate(removeTeam, nextPerson, updatedPeople)
          } else {
            new WorldCupSweep {
              val teams: Seq[Team] = removeTeam
              val people: List[Person] = updatedPeople
            }
          }
        } else {
          new WorldCupSweep {
            val teams: Seq[Team] = removeTeam
            val people: List[Person] = updatedPeople
          }
        }


      }

      iterate()

    }
  }
}

object Teams {

  def apply(): List[Team] = groupA ++ groupB ++ groupC ++ groupD ++ groupE ++ groupF ++ groupG ++ groupH

  val groupA: List[Team] = List(Egypt, Russia, SaudiArabia, Uruguay)
  val groupB: List[Team] = List(Iran, Morocco, Portugal, Spain)
  val groupC: List[Team] = List(Australia, Denmark, France, Peru)
  val groupD: List[Team] = List(Argentina, Croatia, Iceland, Nigeria)
  val groupE: List[Team] = List(Brazil, CostaRica, Serbia, Switzerland)
  val groupF: List[Team] = List(Germany, SouthKorea, Mexico, Sweden)
  val groupG: List[Team] = List(Belgium, England, Panama, Tunisia)
  val groupH: List[Team] = List(Colombia, Japan, Poland, Senegal)
}

case object England extends Team
case object Germany extends Team
case object Brazil extends Team
case object Tunisia extends Team
case object Panama extends Team
case object Belgium extends Team
case object France extends Team
case object Spain extends Team
case object Iran extends Team
case object Russia extends Team
case object Egypt extends Team
case object Uruguay extends Team
case object Portugal extends Team
case object Morocco extends Team
case object SaudiArabia extends Team
case object Australia extends Team
case object Argentina extends Team
case object Iceland extends Team
case object Peru extends Team
case object Denmark extends Team
case object Croatia extends Team
case object Nigeria extends Team
case object CostaRica extends Team
case object Serbia extends Team
case object Mexico extends Team
case object Sweden extends Team
case object Switzerland extends Team
case object SouthKorea extends Team
case object Colombia extends Team
case object Japan extends Team
case object Poland extends Team
case object Senegal extends Team