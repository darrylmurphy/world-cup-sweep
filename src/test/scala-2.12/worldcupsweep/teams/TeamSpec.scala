package worldcupsweep.teams

import org.scalacheck.Gen
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{Inspectors, Matchers, WordSpec}
import worldcupsweep.Person
import TeamGenerator._

class TeamSpec extends WordSpec with Matchers with GeneratorDrivenPropertyChecks {

  "Teams" should {

    "contain group A with 4 teams" in {
      Teams.groupA.length should equal(4)
    }
    "contain group B with 4 teams" in {
      Teams.groupB.length should equal(4)
    }
    "contain group C with 4 teams" in {
      Teams.groupC.length should equal(4)
    }
    "contain group D with 4 teams" in {
      Teams.groupD.length should equal(4)
    }
    "contain group E with 4 teams" in {
      Teams.groupE.length should equal(4)
    }
    "contain group F with 4 teams" in {
      Teams.groupF.length should equal(4)
    }
    "contain group G with 4 teams" in {
      Teams.groupG.length should equal(4)
    }
    "contain group H with 4 teams" in {
      Teams.groupH.length should equal(4)
    }


    def indexGen(max: Int) = for {
      a <- Gen.choose(0, max)
      if a > 0
    } yield {
      a
    }

    def personGen(max: Int) = {
      val people: Gen[Person] = Gen.alphaStr map (alpha =>
        Person(alpha.take(6))
        )
      Gen.listOfN(max, people)
    }

    "equally assign each person a different set of teams" when {

      "teams can be equally divided by the people" when {

        "a single person" in {
          Teams().assignTeams(List(Person("One"))).people.head.teams.length should equal(32)
        }

        "two people" in {
          val people = Teams().assignTeams(List(Person("One"), Person("Two"))).people

          Inspectors.forAll(people.head.teams) { team =>
            people.last.teams should not contain team
          }
        }

        "four people" in {
          val people = Teams().assignTeams(personGen(4).sample.get).people

          Inspectors.forAll(people) { person =>
            person.teams.length should equal(32 / 4)
          }

          forAll(Gen.choose(0, 3), Gen.choose(0, 3)) { (a, b) =>
            whenever(a != b) {
              Inspectors.forAll(people(a).teams) { team =>
                people(b).teams should not contain team
              }
            }
          }

        }

      }

      "teams cannot be equally divided by the people" when {

        "three people" in {

          val sweep = Teams().assignTeams(personGen(3).sample.get)
          val people = sweep.people

          val remainingTeams = 32 % 3
          val teamsAssigned = (32 - remainingTeams) / people.length

          Inspectors.forAll(people) { person =>
            person.teams.length should equal(teamsAssigned)
          }

          forAll(Gen.choose(0, 2), Gen.choose(0, 2)) { (a, b) =>
            whenever(a != b) {
              Inspectors.forAll(people(a).teams) { team =>
                sweep.teams should not contain team
                people(b).teams should not contain team
              }
            }
          }

        }

        "five people" in {

          val sweep = Teams().assignTeams(personGen(5).sample.get)
          val people = sweep.people

          val remainingTeams = 32 % 5
          val teamsAssigned = (32 - remainingTeams) / people.length

          Inspectors.forAll(people) { person =>
            person.teams.length should equal(teamsAssigned)
          }

          forAll(indexGen(4), indexGen(4)) { (a, b) =>
            whenever {
              a > 0 & b > 0 & a != b
            } {
              Inspectors.forAll(people(a).teams) { team =>
                sweep.teams should not contain team
                people(b).teams should not contain team
              }
            }
          }

        }

        "fifteen people" in {

          val sweep = Teams().assignTeams(personGen(15).sample.get)
          val people = sweep.people

          val remainingTeams = 32 % 15
          val teamsAssigned = (32 - remainingTeams) / people.length

          Inspectors.forAll(people) { person =>
            person.teams.length should equal(teamsAssigned)
          }

          forAll(indexGen(14), indexGen(14)) { (a, b) =>
            whenever(a != b) {
              Inspectors.forAll(people(a).teams) { team =>
                sweep.teams should not contain team
                people(b).teams should not contain team
              }
            }
          }
        }

      }

    }

  }

}
